# docker compose configuration for <a href='https://minerva.pages.uni.lu/doc/'>minerva platform</a>

## Quickstart guide

To make it running just clone the repository and run docker-compose:

```
git clone https://git-r3lab.uni.lu/minerva/docker.git
cd docker
docker-compose up
```

## Upgrade to the latest version

Check https://webdav-r3lab.uni.lu/public/minerva/ for the latest version available and modify `.env` file to use it. For instance to use minerva 14.0.4 version the .env file should have entry:
```
WAR_URL=https://webdav-r3lab.uni.lu/public/minerva/14.0.4/minerva.war
```

